﻿using System.Data.Entity;
using WebParking.Models;

namespace WebParking.DataBase
{
    public class ParkingContext : DbContext
    {
        public ParkingContext() : base("Parking1")
        {
            Database.SetInitializer<ParkingContext>(new DropCreateDatabaseIfModelChanges<ParkingContext>());
        }
        public DbSet<ParkingSpot> ParkingSpots { get; set; }
        public DbSet<Car> Cars { get; set; }
        public DbSet<CarHistory> History { get; set; }
    }
}