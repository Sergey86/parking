namespace WebParking.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ValidationModels : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ParkingSpots", "CarId", "dbo.Cars");
            DropIndex("dbo.ParkingSpots", new[] { "CarId" });
            AlterColumn("dbo.Cars", "CarNumber", c => c.String(nullable: false));
            AlterColumn("dbo.Cars", "CarModel", c => c.String(nullable: false));
            AlterColumn("dbo.ParkingSpots", "CarId", c => c.Int(nullable: true));
            CreateIndex("dbo.ParkingSpots", "CarId");
            AddForeignKey("dbo.ParkingSpots", "CarId", "dbo.Cars", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ParkingSpots", "CarId", "dbo.Cars");
            DropIndex("dbo.ParkingSpots", new[] { "CarId" });
            AlterColumn("dbo.ParkingSpots", "CarId", c => c.Int());
            AlterColumn("dbo.Cars", "CarModel", c => c.String());
            AlterColumn("dbo.Cars", "CarNumber", c => c.String());
            CreateIndex("dbo.ParkingSpots", "CarId");
            AddForeignKey("dbo.ParkingSpots", "CarId", "dbo.Cars", "Id");
        }
    }
}
