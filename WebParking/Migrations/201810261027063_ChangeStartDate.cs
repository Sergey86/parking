namespace WebParking.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeStartDate : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ParkingSpots", "StartDate", c => c.DateTime(nullable: true));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ParkingSpots", "StartDate", c => c.DateTime(nullable: false));
        }
    }
}
