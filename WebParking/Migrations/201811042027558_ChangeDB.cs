namespace WebParking.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeDB : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CarHistories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CarModel = c.String(),
                        CarNumber = c.String(),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.CarHistories");
        }
    }
}
