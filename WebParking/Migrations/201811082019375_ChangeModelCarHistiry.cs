namespace WebParking.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeModelCarHistiry : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CarHistories", "ParkingSpotId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CarHistories", "ParkingSpotId");
        }
    }
}
