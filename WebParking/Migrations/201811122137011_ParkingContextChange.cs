namespace WebParking.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ParkingContextChange : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ParkingSpots", "StartDate", c => c.DateTime(nullable: true));
            AlterColumn("dbo.ParkingSpots", "EndDate", c => c.DateTime(nullable: true));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ParkingSpots", "EndDate", c => c.DateTime());
            AlterColumn("dbo.ParkingSpots", "StartDate", c => c.DateTime());
        }
    }
}
