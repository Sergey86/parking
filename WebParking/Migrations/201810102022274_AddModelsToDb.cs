namespace WebParking.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddModelsToDb : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cars",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CarNumber = c.String(),
                        CarModel = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ParkingSpots",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsFree = c.Boolean(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(),
                        CarId = c.Int(nullable: true),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cars", t => t.CarId)
                .Index(t => t.CarId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ParkingSpots", "CarId", "dbo.Cars");
            DropIndex("dbo.ParkingSpots", new[] { "CarId" });
            DropTable("dbo.ParkingSpots");
            DropTable("dbo.Cars");
        }
    }
}
