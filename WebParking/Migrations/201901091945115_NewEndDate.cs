namespace WebParking.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewEndDate : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ParkingSpots", "EndDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ParkingSpots", "EndDate", c => c.DateTime(nullable: false));
        }
    }
}
