﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web.Mvc;
using WebParking.DataBase;
using WebParking.Models;


namespace WebParking.Controllers
{
    public class HomeController : Controller
    {
        ParkingContext parkingContext;

        public HomeController()
        {
            parkingContext = new ParkingContext();
        }

        [HttpGet]
        public ActionResult GetAllParkingSpots()
        {
            var parkingSpots2 = parkingContext.ParkingSpots
                .Include(p => p.Car);
            return View(parkingSpots2.ToList());
        }

        [HttpGet]
        public ActionResult GetAllCars()
        {
            var AllCars = parkingContext.Cars
                .ToList()
                .Where(p => p.CarNumber != null);
            return PartialView(AllCars);
        }

        [HttpGet]
        public ActionResult EditCarAtParkingSpot(int id)
        {
            var GetFreeParkingSpot = parkingContext.ParkingSpots
                .Include(p => p.Car)
                .FirstOrDefault(p => p.Id == id);
            return View(GetFreeParkingSpot);
        }

        [HttpPost]
        public ActionResult Save(ParkingSpot parkingSpot)
        {
            // select a parking spot from Db
            var spot = parkingContext.ParkingSpots
                .Include(p => p.Car)
                .FirstOrDefault(p => p.Id == parkingSpot.Id);

            if (spot.StartDate == null)
            {
                spot.StartDate = parkingSpot.StartDate;
                spot.CarId = parkingSpot.CarId;
                spot.IsFree = false;
                spot.EndDate = null;
                parkingContext.SaveChanges();
                return RedirectToAction("GetAllParkingSpots");
            }

            if (spot.StartDate > parkingSpot.EndDate)
            {
                parkingSpot.EndDate = null;
                return RedirectToAction("EditCarAtParkingSpot", new { id = parkingSpot.Id });
            }
            else
            {
                HistoryCar(parkingSpot);
                spot.StartDate = null;
                spot.EndDate = null;
                spot.CarId = null;
                spot.IsFree = true;
                parkingContext.SaveChanges();
            }

            return RedirectToAction("GetAllParkingSpots");

        }

        public void HistoryCar(ParkingSpot parkingSpot)
        {
            var carTicket = new CarHistory
            {
                ParkingSpotId = parkingSpot.Id,
                CarModel = parkingSpot.Car.CarModel,
                CarNumber = parkingSpot.Car.CarNumber,
                StartDate = (DateTime)parkingSpot.StartDate,
                EndDate = (DateTime)parkingSpot.EndDate
            };
            parkingContext.History.Add(carTicket);
        }

        [HttpGet]
        public ActionResult AddNewCar()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SaveNewCar(Car car)
        {
            if (ModelState.IsValid)
            {
                parkingContext.Cars.Add(car);
                parkingContext.SaveChanges();
                return RedirectToAction("GetAllParkingSpots");
            }
            else
            {
                return View("AddNewCar");
            }
        }

        [HttpGet]
        public ActionResult AddNewParkingSpot()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SaveNewParkingSpot(ParkingSpot parkingSpot)
        {
            if (ModelState.IsValid)
            {
                parkingContext.ParkingSpots.Add(parkingSpot);
                parkingContext.SaveChanges();
                return RedirectToAction("GetAllParkingSpot");
            }
            else
            {
                return View("AddNewParkingSpot");
            }
        }
        [HttpGet]
        public ActionResult HistoryCars()
        {
            var History = parkingContext.History
                .ToList()
                .Where(p => p.Id != 0);
            return View(History);
        }
    }
}