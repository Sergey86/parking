﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace WebParking.Models
{
    public class Car
    {
        [Key]
        [Display(Name = "Car ID")]
        public int Id { get; set; }

        [Display(Name = "Car Number")]
        [Required(ErrorMessage = "Please enter a Car Number")]
        public string CarNumber { get; set; }

        [Display(Name = "Car Model")]
        [Required(ErrorMessage = "Please enter a Car Model")]
        public string CarModel { get; set; }
    }
}