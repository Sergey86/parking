﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebParking.Models
{
    public class ValidationForDate:ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var parkingSpot = (ParkingSpot)validationContext.ObjectInstance;
            var day = parkingSpot.StartDate.Value.Day - parkingSpot.EndDate.Value.Day;
            //if (day > 0)
            //{
            //    new ValidationResult("Endate is incorrect");
            //}
            //else
            //    return ValidationResult.Success;
            return (day < 0)
                 ? ValidationResult.Success
                 : new ValidationResult("Endate is incorrect");
        }
    }
}