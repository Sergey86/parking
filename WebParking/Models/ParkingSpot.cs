﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebParking.Models
{
    public class ParkingSpot
    {
        public int Id { get; set; }
        public bool IsFree { get; set; }
        public DateTime? StartDate { get; set; }
        [ValidationForDate]
        public DateTime? EndDate { get; set; }
        public int? CarId { get; set; }
        public Car Car { get; set; }
    }
}