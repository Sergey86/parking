﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebParking.Models
{
    public class CarHistory
    {
        public int Id { get; set; }
        public int ParkingSpotId { get; set; }
        public string CarModel { get; set; }
        public string CarNumber { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}